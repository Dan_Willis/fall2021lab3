//Name: Dan Willis Njoumene Douanla        ID: 2033804
package LinearAlgebra;
public class Vector3d{

    private double x;
    private double y;
    private double z;


    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+ Math.pow(this.z, 2));
    }

    public double dotProduct(Vector3d vector){
        return (this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ());
    }

    public Vector3d add(Vector3d vector){
        double xAxis = this.x + vector.getX();
        double yAxis =  this.y + vector.getY();
        double zAxis = this.z + vector.getZ();
        Vector3d newVector = new Vector3d(xAxis, yAxis, zAxis);
        return newVector;
    }
}