//Name: Dan Willis Njoumene Douanla        ID: 2033804
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestVector3d {
    
    @Test
    public void testGetX(){
        Vector3d vector = new Vector3d(2,3,4);
        assertEquals(2, vector.getX());
    }

    @Test
    public void testGetY(){
        Vector3d vector = new Vector3d(2,3,4);
        assertEquals(3, vector.getY());
    }

    @Test
    public void testGetZ(){
        Vector3d vector = new Vector3d(2,3,4);
        assertEquals(4, vector.getZ());
    }

    @Test
    public void testMagnitude(){
        Vector3d vector1 = new Vector3d(2,3,4);
        assertEquals(Math.sqrt(29), vector1.magnitude());

        Vector3d vector2 = new Vector3d(1,2,3);
        assertEquals(Math.sqrt(14), vector2.magnitude());
    }

    @Test
    public void testDotProduct(){
        Vector3d vector1 = new Vector3d(1,2,3);
        Vector3d vector2 = new Vector3d(4,5,6);
        assertEquals(32, vector1.dotProduct(vector2));

        Vector3d vector3 = new Vector3d(1,1,2);
        Vector3d vector4 = new Vector3d(2,3,4);
        assertEquals(13, vector3.dotProduct(vector4));
    }

    @Test
    public void testAdd(){
        Vector3d vector1 = new Vector3d(1,2,3);
        Vector3d vector2 = new Vector3d(4,5,6);
        assertEquals(5, vector1.add(vector2).getX());
        assertEquals(7, vector1.add(vector2).getY());
        assertEquals(9, vector1.add(vector2).getZ());

        Vector3d vector3 = new Vector3d(1,1,2);
        Vector3d vector4 = new Vector3d(2,3,4);
        assertEquals(3, vector3.add(vector4).getX());
        assertEquals(4, vector3.add(vector4).getY());
        assertEquals(6, vector3.add(vector4).getZ());
    }
}
